var express = require('express');
var app = express();
var cors = require('cors');

const swaggerDoc = require('./app/config/swaggerDoc');


var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())
app.use(cors());

require('./app/route/fastfood.route.js')(app);
swaggerDoc(app);

// Create a Server
var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("App listening at http://%s:%s", host, port)
})
export default app;