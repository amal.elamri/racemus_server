// Import the dependencies for testing
import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../../server';

// Configure chai
chai.use(chaiHttp);
chai.should();

describe("Fastfoods", () => {

    describe("POST /", () => {

        // Test to get all fasfoods according to state code
        it("should get all fasfoods records according to state code", (done) => {

            chai.request(app)
                .post('/api/fastfoods')
                .set("Content-Type", "application/json")
                .send({ 'code': 'ak' })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });

        // Test to get all fasfoods according to state code
        it("should not get fasfoods records", (done) => {

            chai.request(app)
                .post('/api/fastfoods')
                .set("Content-Type", "application/json")
                .send({ 'code': 'am' })
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        });
    });
});

