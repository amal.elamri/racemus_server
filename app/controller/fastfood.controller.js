var fs = require("fs");
var parse = require("csv-parse");
var csvFile = "./mcdonalds.csv";

class FastFood {
    constructor(longitude, latitude, title, address) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.title = title;
        this.address = address;
    }
}

exports.findAll = (req, res) => {
    //read data from csv
    fs.createReadStream(csvFile)
        .pipe(parse({ delimiter: ',' }, function (err, data) {
            if (err) {
                console.log(`An error was encountered: ${err}`);
                return;
            }

            const fastFoodList = data.map(row => new FastFood(...row));
            // fetch FastFoods
            fetchFastFoods(fastFoodList, req, res);
        }));

}
const fetchFastFoods = (fastFoodList, req, res) => {
    if (req.body.code.length !== 2) {
        return res.status(404).send({
            status: "Fail",
            content: "State code not found"
        });
    }
    var data = []
    fastFoodList.forEach(fastFood => {
        if (fastFood.title.includes(req.body.code.toUpperCase()) == true) {
            data.push(fastFood)
        }
    });
    try {
        if (data.length !== 0) {
            res.status(200).send({
                status: "Success",
                content: data
            });
        }
        else {
            res.status(404).send({
                status: "Fail",
                content: "State code not found"
            });
        }
    }
    catch (error) {
        res.status(500).send({
            status: "Fail",
            message: error
        });

    }
}