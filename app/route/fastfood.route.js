module.exports = function (app) {

    const fastFood = require('../controller/fastfood.controller.js');
   /**
     * @swagger
     * /api/fastfoods:
     *   post:
     *     description: Fetch all fastfoods according to state code
     *     tags: [FastFood]
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: "body"
     *         name: "code"
     *         description: "State Code - Required"
     *         required: true
     *         schema:
     *           type: object
     *           properties:
     *             code:          
     *               description: State Code   
     *               required: true  
     *               type: string
     *     responses:
     *       200:
     *         description: State code found
     *       404:
     *         description: State code not found
     *       500:
     *         description: Internal Server Error
     */
    // Fetch All Fastfoods according to state code
    app.post(
        '/api/fastfoods', fastFood.findAll
    );


}