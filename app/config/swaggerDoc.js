const swaggerJsdoc = require('swagger-jsdoc');
var swaggerUi = require("swagger-ui-express");

const options = {
  swaggerDefinition: {
   
    info: {
      title: 'Fastfood API',
      version: '1.0.0',
      description: 'Fastfood Express API with autogenerated swagger doc',
    },
  },
  // List of files to be processes. You can also set globs './routes/*.js'
  apis: ['app/route/*.js'],
};

const specs = swaggerJsdoc(options);

module.exports= (app) => {
    app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(specs) )
}